package app;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            boolean res = app.approval("2147483647");
            assertTrue(res,() -> "Bigger than int max size needs approval");
        });
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        assertThrows(IllegalArgumentException.class, () -> {
            Main app = new Main();
            boolean res = app.approval("-2147483659");
            assertTrue(res,() -> "Less than int min size needs approval");
        });
    }
}

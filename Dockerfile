FROM gradle:5.4.1-jdk8-alpine as build
ARG GRADLETASK=build
COPY --chown=gradle:gradle . /home/gradle
WORKDIR /home/gradle
ENV GRADLE_USER_HOME /home/gradle
COPY --chown=gradle:gradle . /home/gradle
RUN gradle --info $GRADLETASK -g/home/gradle

FROM openjdk:8-jre-slim
COPY --from=build /home/gradle/build/libs/app.jar /app/
WORKDIR /app
CMD java -jar app.jar
